<?php

include "commandes/mycd.php";
include "commandes/analysecommande.php";
include "commandes/myhelp.php";
include "commandes/myls.php";
include "commandes/helpers/couleurs.php";
include "commandes/Mymv.php";
include "commandes/mycp.php";
include "commandes/helpers/getos.php";
include "commandes/dir.php";

$command_options = [];
$command_args = [];
$commandLine = null;

$path=getcwd();


while (true) {
$line = readlineWithColor (" MiniShell  $path >",COLOR_GREEN);
analyseCommande($line , $commandLine, $command_args, $command_options);

$commandLine=strtolower($commandLine);

if ($commandLine=="mymkdir"){
    MyMkdir($command_args);

}
elseif ($commandLine=="myrm"){
    MyRM($command_args);
}
elseif ($commandLine=="mypwd"){
    getPwd ($path);
}
elseif($commandLine=="mycp"){
    mycp($command_args);
}
elseif($commandLine=="myls"){
    myLs($commandLine,$command_args,$command_options,$path);
}
elseif($commandLine=="mycd"){
    //tester si le nombre d'element === 1
    mycd($path,$command_args[0]);
}
elseif($commandLine == "mymv"){
    Mymv($command_args);
}
elseif ($commandLine=="myhelp"){
    myHelp();
}
elseif($commandLine=='myps'){
    Myps();
}
elseif($commandLine=="exit"){
echo "Au revoir";
break;
}
}