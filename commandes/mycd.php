<?php

function calculerChemin(string $cheminEnCours, string $chemindestination)
{

    //si windows 
    if (getOS() === "Windows") {
        $patternAbsolu = "#^[a-z]:.*$#i";
    } else {
        $patternAbsolu = "#^/.*$#i";
    }
    $cheminEnCours = str_replace("\\", "/", $cheminEnCours);
    $chemindestination = str_replace("\\", "/", $chemindestination);
    if (preg_match($patternAbsolu, $chemindestination)) {
        if (!file_exists($chemindestination))
            return false;
        return $chemindestination;
    }

    $partieChemin = explode("/", $chemindestination);
    foreach ($partieChemin as $unePartie) {
        if (preg_match("#^\.{2}.*#", $unePartie)) {
            $cheminEnCours = dirname($cheminEnCours, 1);
        } else if (preg_match("#^[^.]+$#i", $unePartie)) {
            $cheminEnCours = $cheminEnCours . "/" . $unePartie;  
        }
        $cheminEnCours = str_replace("\\", "/", $cheminEnCours);
        $cheminEnCours = str_replace("//", "/", $cheminEnCours);
    }

    if (!file_exists($cheminEnCours))
        return false;

    return $cheminEnCours;
}
function mycd(string &$path, string $chemin)
{
    $nouveauChemin = calculerChemin($path, $chemin);
    if ($nouveauChemin === false) {
        echoWithColor ("chemin invalide!!!!!\n",COLOR_RED);
    } else {
        $path = $nouveauChemin;
    }
}
