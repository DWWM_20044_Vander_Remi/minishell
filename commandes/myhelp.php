<?php



function myhelp(){

    echo ("\n                                       ---INFOS COMMANDES---                           \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------MyLs---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("  Myls : Afficher le contenu du répertoire passer en argument.\n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("      -l  : Afficher une liste détaillée des caractéristiques de chaque fichier du répertoire.\n");
    echo("      -il : Afficher ls-i + ls-l \n");
    echo("      -i  : Afficher le numéro d'index de chaque fichier à gauche de son nom.\n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mypwd---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("    Mypwd : Affiche le chemin en cours. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mycd---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("     Mycd : Permet de revenir au répertoire. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("    cd -  : Permet de revenir au répertoire précédent. \n");
    echo("    cd .. : Permet de remonter au répertoire parent \n");
    echo("    cd /  : permet de remonter à la racine de l'ensemble du système de fichiers. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mycp---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("    Mycp  : Permet de faire la copie d'un ou plusieurs fichier mais aussi d'un ou plusieurs répertoire. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("   -r, -R : Permet de copier récursivement les répertoires. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mymv---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("    Mymv  : Permet de déplacer un fichier ou répertoire. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Myfind---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("   Myfind : Permet de déplacer un fichier ou répertoire. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("   -name  : Recherche par nom de fichier. \n");
    echo("   -type  : Recherche par type de fichier. \n");
    echo("   -size  : Recherche par taille de fichier. \n");
    echo("   -atime : Recherche par date de dernier accès. \n");
    echo("   -mtime : Recherche par date de dernière modification. \n");
    echo("   -ctime : Recherche par date de création. \n");
    echo("   -user  : Recherche par propriétaire. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mymkdir---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("  Mymkdir : Permet de crée un répertoire correspondant à chacun des noms mentionnés. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("    -p    : Assure que chaque répertoire indiqué existe. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Mytail---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("   Mytail : Affiche la dernière partie (par défaut :  10 lignes) de chacun des fichiers indiqués.  \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("    -n=N  : Afficher N dernières lignes. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Myrm---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("     Myrm : Efface chaque fichier indiqué. Par défaut, il n'efface pas les répertoires. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("      -d  : Efface un répertoire avec `unlink' à la place de `rmdir'. \n");
    echo("      -i  : Demander à l'utilisateur de confirmer l’effacement de chaque fichier. \n");
    echo("-r ou -R  : Supprimer récursivement le contenu des répertoires. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Myhelp---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("   Myhelp : Afficher les commandes disponibles sur le mini shell. \n");
    echo(PHP_EOL);
    echoWithColor("\n     ----------Myps---------- \n",COLOR_LIGHT_GREEN);
    echo(PHP_EOL);
    echo("     Myps : Présente un cliche instantané des processus en cours. \n");
    echoWithColor ("\n OPTIONS : \n",COLOR_LIGHT_RED);
    echo("      -a  : Présente également les processus des  autres utilisateurs. \n");
    echo("      -u  : Présente le  nom  de  l'utilisateur et l'heure de lancement. \n");
    echo("      -x  : Affiche  les  processus  qui n'ont pas de terminal de contrôle. \n");

}

?>