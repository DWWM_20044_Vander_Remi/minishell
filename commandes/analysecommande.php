<?php

function analyseCommande($line, &$commandLine, &$command_args, &$command_options, $est_decoupage_options = true)
{
    $line = trim($line);
    $commandLine = null;
    $command_args = [];
    $command_options = [];
    $matches = [];
    $patternArgs = '([^ -]+([-]?[^ -]*)*[-]?)|(?:(?:"[^"]+")|(?:\'[^\']+\'))';
    $estValide = preg_match("#^([A-Za-z]\w*) *((?:(?: +(?:(?:-[A-Za-z0-9]+)|" . $patternArgs . "))+)*)$#", $line, $matches);

    if ($estValide) {
        $commandLine = $matches[1];
        $subject = preg_split('/[\s]*(\\"[^\\"]+\\")[\s]*|' . '[\s]*(\'[^\']+\')[\s]*|' . '[\s,]+/', trim($matches[2]), 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        //récuperer les options
        $command_options = recupererOptions($subject, $est_decoupage_options);
        //récuperer les arguments
        $command_args = recupererArguments($subject);
    } else {
        echoWithColor("Découpage invalide\n", COLOR_RED);
    }
}
function recupererArguments($subject)
{
    $patterns = ['#^\'([^\']+)\'$#', '#^"([^"]+)"$#', '#^[^ -]+([-]?[^ -]*)*[-]?$#'];
    $replaces = ["$1", "$1", "$0"];
    $command_args = preg_filter($patterns, $replaces, $subject);
    $command_args = array_values($command_args);
    return $command_args;
}
function recupererOptions($subject, $est_decoupage_options)
{
    $patternOptions = '#^-([A-Za-z0-9]+)$#';
    $replaceOptions = '$1';
    $command_options_tmp = preg_filter($patternOptions, $replaceOptions, $subject);
    $command_options = [];
    if ($est_decoupage_options) {
        foreach ($command_options_tmp as $option) {;
            $command_options = array_merge($command_options, str_split($option));
        }
    } else {
        $command_options =  $command_options_tmp;
    }
    return $command_options;
}
?>