<?php
function getOs(){
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $os="Windows";
    } else {
        $os="Linux";
    }
    return $os;
}
?>